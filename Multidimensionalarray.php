<html>
   <body>
      
      <?php
         $marks = array( 
            "Varsi" => array (
			      "physics" => 58,
                   "maths" => 65,	
                 "chemistry" => 50
            ),
            
            "Mallika" => array (
               "physics" => 55,
               "maths" => 55,
               "chemistry" => 46
            ),
            
            "Lalli" => array (
               "physics" => 60,
               "maths" => 70,
               "chemistry" => 40
            )
         );
         
         /* Accessing multi-dimensional array values */
         echo "Marks for Varsi in physics : " ;
         echo $marks['Varsi']['physics'] . "<br />"; 
         
         echo "Marks for Mallika in maths : ";
         echo $marks['Mallika']['maths'] . "<br />"; 
         
         echo "Marks for Lalli in chemistry : " ;
         echo $marks['Lalli']['chemistry'] . "<br />"; 
      ?>
   
   </body>
</html>